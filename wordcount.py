__author__ = 'Michael Abdalov'

import sys

def dict_of_words(filename):
    """
    Returns a dictionary that the keys are the words in the text, and their amounts.

    :param filename: The source of the text file to count the words from.
    :return: The dictionary of the words as keys and their amount in the text.
    """
    # Creates a dictionary object
    count_dic = dict()

    with open(filename,'r') as file_src:
        # itterates over every line in the file
        for line in file_src:

            # Stripes the start of the line from spaces and newlines
            line = line.strip()

            # Filters the line from non-alphanumeric characters
            line = ''.join([char if char.isalnum() or char == ' ' else '' for char in line])

            # Converts every character in the line to lowercase
            line = line.lower()

            # Splits the line into words
            words = line.split(' ')

            # Itterates in each word in the line
            for word in words:
                # doesn't count spaces or blanks inside the line.
                if word not in ['', ' ']:
                    # If the key exists, it adds 1 to the count
                    if word in count_dic:
                        count_dic[word] += 1
                    # Else it will create new kwy in the dictionary
                    else:
                        count_dic[word] = 1

    # Returns that dictionary
    return count_dic

def print_words(filename, option):
    """
    Prints all the words and their amount in the text.

    :param filename: The text file to get text from.
    :return: None.
    """
    dict_words = dict_of_words(filename)

    # Sorts the words according to the option.
    if option == '--apearance':
        dict_words = sort_by_amount(dict_words)
    
    # Lists all the keys, words, to one list.
    list_of_words = list(dict_words.keys())
    
    if option == '--letter':
        list_of_words.sort()
        
    # Itterates over all the words in the dictionary
    for key in list_of_words:
        print(key, dict_words[key])

sort_by_amount = lambda word_dic: {key: val for key, val in sorted(word_dic.items(), key=lambda item: item[1], reverse = True)}
sort_by_alphabet = lambda key_list: key_list.sort()

def print_top(filename):
    """
    Prints the top 20 most mentioned words in the text in the file.
    :param filename: The file the text is from.
    :return: None.
    """
    dict_words = dict_of_words(filename)

    # Sorts the dictionary from the biggest value to the smallest.
    dict_words = sort_by_amount(dict_words)
    list_of_all_words = list(dict_words.keys())

    top_20_words = []

    # Puts the top 20 words in one list
    for key in list_of_all_words:
        if len(top_20_words) < 20:
            top_20_words.append(key)
        else:
            break

    # Itterates only over the top 20 words after the filter
    for key in top_20_words:
        print(key, dict_words[key])


def main():
    option = ''
    sub_option = 'None'
    filename = ''

    if len(sys.argv) == 3:
        option = sys.argv[1]
        filename = sys.argv[2]

    elif len(sys.argv) == 4 :
        option = sys.argv[1]
        sub_option = sys.argv[2]
        filename = sys.argv[3]
    else:
        print("usage: ./wordcount.py {--count {--appearance|--letter}| ---topcount} file")
        sys.exit(1)

    if option == '--count' and sub_option in ['--appearance','--letter']:
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
