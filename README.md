##**Word Counter in python**

wordcount.py {--count {--letter| --apearance}|--topcount|--nameprint} file_name

###Description

The program counts every word in a given file.

For the '''--count''' flag the program prints to the console every word and it's amount, needs one of two optional flags.
The '''--letter''' flag, it will print all the word in an alphabetical order, for the '''--apearance''', it will print that list from the biggest amount to the smallest. 

For the '''--topcount''' flag  the program prints the top 20 most mentioned words with their amounts sorted from the biggest amount to the 20th amount.

